package main

import (
	"flag"
	"io"
	"log"
	"os"
	"os/exec"

	"github.com/joncalhoun/pipe"
)

type data struct {
	Pkg  string
	Name string
	Type string

	RenderCursorTemplate bool
}

func main() {
	var d data
	flag.StringVar(&d.Pkg, "pkg", "main", "The name of the package.")
	flag.StringVar(&d.Name, "name", "", "The name of the type to generate for.")
	flag.StringVar(&d.Type, "type", "", "The type to generate for.")
	flag.BoolVar(&d.RenderCursorTemplate, "cursor", false, "Generate cursor template")
	flag.Parse()

	if d.Pkg == "" {
		log.Fatalf("pkg is required")
	}
	if d.Name == "" {
		log.Fatalf("name is required")
	}
	if d.Type == "" {
		log.Fatalf("type is required")
	}

	p, err := pipe.New(
		exec.Command("gofmt"),
		exec.Command("goimports"),
	)
	if err != nil {
		log.Fatalf("unable to create pipe: %s", err.Error())
	}

	r, w := io.Pipe()
	p.Stdin = r
	p.Stdout = os.Stdout

	if err := p.Start(); err != nil {
		log.Fatalf("unable to start pipe: %s", err.Error())
	}

	if err := nodeTemplate.Execute(w, d); err != nil {
		log.Fatalf("unable to execute node template: %s", err.Error())
	}
	if d.RenderCursorTemplate {
		if err := cursorTemplate.Execute(w, d); err != nil {
			log.Fatalf("unable to execute cursor template: %s", err.Error())
		}
	}
	if err := w.Close(); err != nil {
		log.Fatalf("unable to close pipe writer: %s", err.Error())
	}

	if err := p.Wait(); err != nil {
		log.Fatalf("error waitng for pipe to finish: %s", err.Error())
	}
}
